<?php

namespace Database\Seeders;

use App\Models\Petition;
use Illuminate\Database\Seeder;

class PetitionSeeder extends Seeder
{
    public function run(): void
    {
        Petition::factory()->times(50)->create();
    }
}
