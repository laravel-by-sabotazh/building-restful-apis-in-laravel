## Tutorial

From [here](https://www.linkedin.com/learning/building-restful-apis-in-laravel-8532490/building-restful-apis-in-laravel).

## Postman

[Collections](https://apiresident.postman.co/workspace/My-Workspace~bdac5a9d-79af-4bcf-8172-e7ab22cf4382/collection/18983784-7d3cefaa-7ebc-44b2-8a3b-f31f8fc9ced9).

## Artisan commands

```bash
composer require doctrine/dbal
```
```bash
php artisan make:migration change_category_type_in_petitions
```
```bash
php artisan make:controller PetitionController --api --model=Petition
```
```bash
php artisan db:seed --class=PetitionSeeder
```

## License

Licensed under the [MIT license](https://opensource.org/licenses/MIT).
