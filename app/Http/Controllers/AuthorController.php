<?php

namespace App\Http\Controllers;

use App\Http\Resources\AuthorCollection;
use App\Http\Resources\AuthorResource;
use App\Models\Author;

class AuthorController extends Controller
{
    public function index(): AuthorCollection
    {
        return new AuthorCollection(Author::all());
    }

    public function show(Author $author): AuthorResource
    {
        return new AuthorResource($author);
    }
}
